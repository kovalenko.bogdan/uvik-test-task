# uvik-colorboard

## Description

- This is test task for Uvik. 
- This project was generated with [Cookiecutter Django](https://cookiecutter-django.readthedocs.io/en/latest/index.html).
- Django 2.0.13

## Requirements

- `python3.6` or higher
- `python3-venv`
- `PostgreSQL 10.7` or higher

## Local Installation

 - `git clone git@gitlab.com:kovalenko.bogdan/uvik-test-task.git`
 - `cd uvik-test-task`
 - `python3 -m venv venv`
 - `source venv/bin/activate`
 - `pip install -r requirements/local.py`

## Development server
- `python manage.py migrate`
- `python manage.py runserver`


## Deploy
[Link](https://cookiecutter-django.readthedocs.io/en/latest/deployment-on-heroku.html)

Project is currently deployed on Heroku

https://uvik-colorboard.herokuapp.com/

You can see game results saved in database from admin page

https://uvik-colorboard.herokuapp.com/admin/

(You can get credentials from author)


## Authors and acknowledgment

Dev:

- Bogdan Kovalbogdan @kovalenko.bogdan

