from django.contrib import admin
from .models import GameResults

admin.site.register(GameResults)
