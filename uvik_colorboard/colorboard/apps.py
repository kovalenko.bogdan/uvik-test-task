from django.apps import AppConfig


class ColorboardConfig(AppConfig):
    name = 'uvik_colorboard.colorboard'
