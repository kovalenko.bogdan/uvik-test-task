from django.forms import Form, IntegerField, CharField, NumberInput, Textarea
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator


class ColorboardForm(Form):
    number_of_players = IntegerField(
        label='The​ ​number​ ​of​ ​players',
        initial=2,
        validators=[MinValueValidator(1),
                    MaxValueValidator(4)],
        widget=NumberInput(attrs={'class': 'form-control'}))

    squares_on_board = IntegerField(
        label='The​ ​number​ ​of​ ​squares​ ​on​ ​the​ ​board​',
        initial=13,
        validators=[MinValueValidator(1),
                    MaxValueValidator(79)],
        widget=NumberInput(attrs={'class': 'form-control'}))

    number_cards_in_the_deck = IntegerField(
        label='The​ ​number​ ​of​ ​cards​ ​in​ ​the​ ​deck',
        initial=8,
        validators=[MinValueValidator(1),
                    MaxValueValidator(200)],
        widget=NumberInput(attrs={'class': 'form-control'}))

    characters_on_the_board = CharField(
        label='The​ ​characters​ ​representing​ ​the​ ​colored​ ​squares​ ​on​ ​the​ ​board',
        initial='RYGPBRYGBRPOP',
        validators=[RegexValidator(regex="^[A-Z]+$", message="Allowed only uppercase A-Z ")],
        widget=Textarea(attrs={'class': 'form-control', 'rows': 4}))

    cards_in_the_deck = CharField(
        label='The​ ​cards​ ​in​ ​the​ ​deck​',
        initial='R, B, GG, Y, P, B, P, RR',
        validators=[RegexValidator(regex=r"^[A-Z,\s]+$",
                                   message="Allowed only uppercase A-Z comma and space"), ],
        widget=Textarea(attrs={'class': 'form-control', 'rows': 4}))
