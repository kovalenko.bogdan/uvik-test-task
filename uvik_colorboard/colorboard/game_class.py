class Game(object):
    """Game logic class, that creates object with given game parameters
    and implements game logic in start_game method"""

    def __init__(self, number_of_players, squares_on_board, number_cards_in_the_deck,
                 characters_on_the_board, cards_in_the_deck):
        self.number_of_players = number_of_players
        self.squares_on_board = squares_on_board
        self.number_cards_in_the_deck = number_cards_in_the_deck
        self.characters_on_the_board = characters_on_the_board
        self.cards_in_the_deck = cards_in_the_deck

    def start_game(self):
        """Runs game with initialized data"""

        # Create array with cards
        cards = self.cards_in_the_deck.split(',')
        # Create dictionary with initial position of -1 for all players
        player_position = dict((x, -1) for x in range(1, self.number_of_players + 1))

        if len(cards) != self.number_cards_in_the_deck:
            return 'Wrong number of cards in the deck.'

        # Game step loop
        game_data_generator = ((index, (index % self.number_of_players) + 1, item) for index, item in enumerate(cards))
        for index, player, card in game_data_generator:

            # Iterates through double card, like "GG" etc.
            for item in card.strip():

                # Get current position of the current player
                position = player_position[player]
                # Find card starting from player current position
                found = self.characters_on_the_board.find(item, position + 1)

                # Check WIN conditions
                # If card not found after player position of was found last card on the board - WIN
                if found < 0 or found + 1 == self.squares_on_board:
                    return 'Player %s won after %s cards.' % (player, index + 1)
                # Update position of the current user
                player_position[player] = found

        # Draw if cards empty and there are no winner
        return 'No player won after %s cards.' % self.number_cards_in_the_deck
