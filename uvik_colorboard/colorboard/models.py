from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class GameResults(models.Model):
    number_of_players = models.IntegerField(validators=[
            MaxValueValidator(4),
            MinValueValidator(1)
        ])
    squares_on_board = models.IntegerField(validators=[
            MaxValueValidator(79),
            MinValueValidator(1)
        ])
    number_cards_in_the_deck = models.IntegerField(validators=[
            MaxValueValidator(200),
            MinValueValidator(1)
        ])
    characters_on_the_board = models.CharField(max_length=400)
    cards_in_the_deck = models.CharField(max_length=400)
    result = models.CharField(max_length=50)

    def __str__(self):
        return self.result
