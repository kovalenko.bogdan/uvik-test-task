from django.shortcuts import render
from django.views import View
from .forms import ColorboardForm
from .game_class import Game
from .models import GameResults


class ColorBoardView(View):
    def get(self, request):
        """Renders page with form and prints result to the user"""

        form = ColorboardForm()
        result = None
        return render(request, 'colorboard/home.html', {'form': form, 'result': result})

    def post(self, request):
        """Validates data from form and runs game with given data"""

        form = ColorboardForm(request.POST)
        result = None
        if form.is_valid():
            data = form.cleaned_data
            game = Game(**data)
            result = game.start_game()

            GameResults(result=result, **data).save()

        return render(request, 'colorboard/home.html', {'form': form, 'result': result})
